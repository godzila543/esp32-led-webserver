#include <NeoPixelBus.h>

#define strip_PixelCount 300
#define strip1_PixelPin 22
#define strip2_PixelPin 25
#define strip3_PixelPin 26

#define totalPixelCount 900



NeoPixelBus<NeoGrbFeature, NeoEsp32Rmt0Ws2812xMethod> strip1(strip_PixelCount, strip1_PixelPin);
NeoPixelBus<NeoGrbFeature, NeoEsp32Rmt1Ws2812xMethod> strip2(strip_PixelCount, strip2_PixelPin);
NeoPixelBus<NeoGrbFeature, NeoEsp32Rmt2Ws2812xMethod> strip3(strip_PixelCount, strip3_PixelPin);

NeoBuffer<NeoBufferMethod<NeoGrbFeature>> strip0(totalPixelCount,1,NULL);

NeoTopology<RowMajorLayout> topo(totalPixelCount, 1);
uint16_t LayoutMap(int16_t x, int16_t y)
{
  return topo.MapProbe(x, y);
}


enum genType
{
  GRADIENT, PARTICLE
};
struct preset
{
  genType gen = GRADIENT;
  float rate = 1.0f;
  float density = 0.5f;
};
struct palette
{
  uint8_t len = 2;
  RgbColor colors[8] = {RgbColor(0,0,0), RgbColor(255,255,255)}; // max 8 colors
};
struct Database 
{
  preset Preset; //Database will only store the active preset and palette. It is up to the browser to send the correct information
  palette Palette; 
  float Time;
};

Database DB;

void JSONtoPalette(char * JSONstr)
{
  DynamicJsonDocument doc(512);
  deserializeJson(doc, JSONstr);
  DB.Palette.len=doc["length"];
  
  JsonArray colors = doc["colors"].as<JsonArray>();
  for(int i=0; i<DB.Palette.len; i++) {
    DB.Palette.colors[i] = RgbColor(colors[i][0], colors[i][1], colors[i][2]);
  }
  Serial.println(DB.Palette.len);
  Serial.println(DB.Palette.colors[0].R);
}

void JSONtoPreset(char * JSONstr)
{
  DynamicJsonDocument doc(512);
  deserializeJson(doc, JSONstr);
  DB.Preset.gen = doc["generator"];
  DB.Preset.rate = doc["rate"];
  DB.Preset.density = doc["density"];
  Serial.println(DB.Preset.rate);
  Serial.println(DB.Preset.density);
  Serial.println(DB.Preset.gen);
}

RgbColor getColorFromPalette(float pos) {
  pos = fmod(pos, 1);
  return RgbColor::LinearBlend(DB.Palette.colors[int(pos * (DB.Palette.len-1))], DB.Palette.colors[int(pos * (DB.Palette.len-1))+1], fmod(pos * (DB.Palette.len-1), 1));
}

RgbColor calculateGradient(float pos) {
  return getColorFromPalette(DB.Preset.density * (pos + DB.Time));
}

void calculateStrip() {
  if (DB.Preset.gen == GRADIENT) {
    for (int i=0; i<900; i++) {
      strip0.SetPixelColor(i, 0, calculateGradient((float)i / 900.0f));
     
      if (i==0) {
//        Serial.println(calculateGradient((float)i / 900.0f).R);
      }
    }
    DB.Time += DB.Preset.rate;
  }
}

void LEDSetup() {
  pinMode(strip1_PixelPin, OUTPUT);
  pinMode(strip2_PixelPin, OUTPUT);
  pinMode(strip3_PixelPin, OUTPUT);
  
  strip1.Begin();
  strip2.Begin();
  strip3.Begin();
  // reset them to off:
  strip1.Show();
  strip2.Show();
  strip3.Show();

  // set the entire buffer to black color to set off for all pixels:
  //strip0.ClearTo(black);
  
}

void LEDLoop() {
  calculateStrip();
  strip0.Blt(strip1, 0, 0, 0                   , 0, strip_PixelCount, 1, LayoutMap);
  strip0.Blt(strip2, 0, 0, strip_PixelCount    , 0, strip_PixelCount, 1, LayoutMap);
  strip0.Blt(strip3, 0, 0, strip_PixelCount * 2, 0, strip_PixelCount, 1, LayoutMap);
  // send pixels from strip0 using Blt, to strip02, starting the dump at strip02's LED x,y coordinates, always 0,0 for our simple use of Blt. From the buffer starting coordinates 18,0 (y will always be 0 for our simple use of Blt. Buffer rectangle size being dumped: 18 pixels wide, 1 pixel tall. This essentially defines how many pixels get dumped. Then using the LayoutMap that reflects the size and shape of the strip00 buffer, since we're only using layout map because its a prerequisite to use the complex Blt method where we can define the source/dest pixels specifically. 
  strip1.Show();
  strip2.Show();
  strip3.Show();
}
