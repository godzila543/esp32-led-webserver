#include <WiFi.h>
#include <SPIFFS.h>
#include <ESPAsyncWebServer.h>
#include <WebSocketsServer.h>
#include <ArduinoJson.h>
#include "Led.h"
#include "Web.h"

void setup() {

  webInit();
  LEDSetup();
}

void loop() {
  
  // Look for and handle WebSocket data
  
  webLoop();
  LEDLoop();
}
